<?php

/* {# inline_template_start #}<div class="product-category product-category2">
<div class="product-category-image"><a href="{{ field_product_category }}">{{ field_image }} </a></div>
<div class="product-category-name-wrap">
<a  class="product-category-url" href="{{ field_product_category }}">
<span class="product-category-name">{{ name }}<span class="product-category-count">{{ field_product_category_1 }}</span></span>
<span class="product-category-action">Shop Now <i class="fa fa-angle-double-right"></i></span>
</a>
</div>
</div> */
class __TwigTemplate_4d1438fc3d0ab7099a119b8430370de91248a1ad209a3acf876881f5ea06cdad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"product-category product-category2\">
<div class=\"product-category-image\"><a href=\"";
        // line 2
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_product_category"] ?? null), "html", null, true));
        echo "\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_image"] ?? null), "html", null, true));
        echo " </a></div>
<div class=\"product-category-name-wrap\">
<a  class=\"product-category-url\" href=\"";
        // line 4
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_product_category"] ?? null), "html", null, true));
        echo "\">
<span class=\"product-category-name\">";
        // line 5
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["name"] ?? null), "html", null, true));
        echo "<span class=\"product-category-count\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_product_category_1"] ?? null), "html", null, true));
        echo "</span></span>
<span class=\"product-category-action\">Shop Now <i class=\"fa fa-angle-double-right\"></i></span>
</a>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"product-category product-category2\">
<div class=\"product-category-image\"><a href=\"{{ field_product_category }}\">{{ field_image }} </a></div>
<div class=\"product-category-name-wrap\">
<a  class=\"product-category-url\" href=\"{{ field_product_category }}\">
<span class=\"product-category-name\">{{ name }}<span class=\"product-category-count\">{{ field_product_category_1 }}</span></span>
<span class=\"product-category-action\">Shop Now <i class=\"fa fa-angle-double-right\"></i></span>
</a>
</div>
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 5,  61 => 4,  54 => 2,  51 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}<div class=\"product-category product-category2\">
<div class=\"product-category-image\"><a href=\"{{ field_product_category }}\">{{ field_image }} </a></div>
<div class=\"product-category-name-wrap\">
<a  class=\"product-category-url\" href=\"{{ field_product_category }}\">
<span class=\"product-category-name\">{{ name }}<span class=\"product-category-count\">{{ field_product_category_1 }}</span></span>
<span class=\"product-category-action\">Shop Now <i class=\"fa fa-angle-double-right\"></i></span>
</a>
</div>
</div>", "");
    }
}
