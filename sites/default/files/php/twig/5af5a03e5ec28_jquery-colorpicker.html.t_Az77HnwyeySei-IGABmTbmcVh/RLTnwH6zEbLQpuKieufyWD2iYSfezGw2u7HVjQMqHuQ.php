<?php

/* modules/contrib/jquery_colorpicker/templates/jquery-colorpicker.html.twig */
class __TwigTemplate_3fc91ba5e51e5bf84978f863b9ad11d781c4ec97540fdbdd0e7a3743c716e484 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 8);
        $filters = array("t" => 5);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array('t'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"jquery_colorpicker\">
\t<div id=\"";
        // line 2
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["id"] ?? null), "html", null, true));
        echo "-inner_wrapper\" class=\"inner_wrapper\">
\t\t<div class=\"color_picker\" style=\"background-color:";
        // line 3
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["default_color"] ?? null), "html", null, true));
        echo ";\">
\t\t\t<span class=\"hash\">#</span><input";
        // line 4
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["attributes"] ?? null), "html", null, true));
        echo " />";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["children"] ?? null), "html", null, true));
        echo "
\t\t\t<div class=\"description\">";
        // line 5
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Enter a hexidecimal color value. Enabling javascript will replace this input with a graphical color selector")));
        echo "</div>
\t\t</div>
\t</div>
\t";
        // line 8
        if (($context["show_remove"] ?? null)) {
            // line 9
            echo "\t\t<div>
\t\t\t<a href=\"#\" class=\"jquery_field_remove_link\">";
            // line 10
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Remove")));
            echo "</a>
\t\t</div>
\t";
        }
        // line 13
        echo "\t";
        if (($context["show_clear"] ?? null)) {
            // line 14
            echo "\t\t<div>
\t\t\t<a href=\"#\" class=\"jquery_field_clear_link\">";
            // line 15
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Clear")));
            echo "</a>
\t\t</div>
\t";
        }
        // line 18
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "modules/contrib/jquery_colorpicker/templates/jquery-colorpicker.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 18,  83 => 15,  80 => 14,  77 => 13,  71 => 10,  68 => 9,  66 => 8,  60 => 5,  54 => 4,  50 => 3,  46 => 2,  43 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modules/contrib/jquery_colorpicker/templates/jquery-colorpicker.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/modules/contrib/jquery_colorpicker/templates/jquery-colorpicker.html.twig");
    }
}
