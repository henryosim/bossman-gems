<?php

/* {# inline_template_start #}<div class="panel-heading">
<span class="panel-title">
<a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapse-{{ nid }}">{{ title }} </a>
</span>
</div>
<div class="panel-collapse collapse" id="collapse-{{ nid }}">
<div class="panel-body">
{{ body }}
</div>
</div> */
class __TwigTemplate_f13a36b59b9f1594e0314a810b479773d5ce6384975312110ca6e01b6f717420 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"panel-heading\">
<span class=\"panel-title\">
<a class=\"collapsed\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapse-";
        // line 3
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["nid"] ?? null), "html", null, true));
        echo "\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
        echo " </a>
</span>
</div>
<div class=\"panel-collapse collapse\" id=\"collapse-";
        // line 6
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["nid"] ?? null), "html", null, true));
        echo "\">
<div class=\"panel-body\">
";
        // line 8
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["body"] ?? null), "html", null, true));
        echo "
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"panel-heading\">
<span class=\"panel-title\">
<a class=\"collapsed\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapse-{{ nid }}\">{{ title }} </a>
</span>
</div>
<div class=\"panel-collapse collapse\" id=\"collapse-{{ nid }}\">
<div class=\"panel-body\">
{{ body }}
</div>
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 8,  64 => 6,  56 => 3,  52 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}<div class=\"panel-heading\">
<span class=\"panel-title\">
<a class=\"collapsed\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapse-{{ nid }}\">{{ title }} </a>
</span>
</div>
<div class=\"panel-collapse collapse\" id=\"collapse-{{ nid }}\">
<div class=\"panel-body\">
{{ body }}
</div>
</div>", "");
    }
}
