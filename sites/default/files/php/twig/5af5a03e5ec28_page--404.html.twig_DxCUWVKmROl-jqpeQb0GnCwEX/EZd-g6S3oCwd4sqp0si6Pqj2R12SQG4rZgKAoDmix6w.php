<?php

/* themes/wosh/templates/layout/page--404.html.twig */
class __TwigTemplate_15c0de5b066fc6e56c3d61878a9f9bebe990641211a78c01d06b73fa8085653f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("include" => 48, "if" => 50);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('include', 'if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 46
        echo "<div id=\"wrapper\" class=\"wrapper ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["site_layout"] ?? null), "html", null, true));
        echo "\">
<div class=\"layout-wrap ";
        // line 47
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["sticky_header"] ?? null), "html", null, true));
        echo "\">
\t";
        // line 48
        $this->loadTemplate("@wosh/layout/header.html.twig", "themes/wosh/templates/layout/page--404.html.twig", 48)->display($context);
        // line 49
        echo "
\t";
        // line 50
        if ($this->getAttribute(($context["page"] ?? null), "slider", array())) {
            // line 51
            echo "\t<!-- Slider -->
\t<section id=\"slider\" class=\"clearfix\">        
\t\t<div id=\"slider-wrap\">
\t\t\t<div class=\"container-fluid\">
\t\t\t\t<div class=\"row\">
\t\t\t\t";
            // line 56
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "slider", array()), "html", null, true));
            echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</section>
\t<!-- End Slider -->
\t";
        }
        // line 63
        echo "

\t";
        // line 65
        if ($this->getAttribute(($context["page"] ?? null), "content_wide_top", array())) {
            // line 66
            echo "\t<!-- Start content top -->
\t<section id=\"content-wide-top\" class=\"content-wide\">        
\t\t";
            // line 68
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content_wide_top", array()), "html", null, true));
            echo "
\t</section>
\t<!-- End content top -->
\t";
        }
        // line 72
        echo "
\t<!-- layout -->
\t<section id=\"page-wrapper\" class=\"page-wrapper\">

\t  <div class=\"container\">\t
\t\t<div class=\"row layout\">
\t\t  <!--- Start content -->
\t\t\t<div class=\"content-layout\">
\t\t\t  <div class=\"col-12\">
\t\t\t\t<div class=\"error-404 text-middle text-center\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-12\">
\t\t\t\t\t<div class=\"error-404-number\">404</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-12\">
\t\t\t\t\t<div class=\"text-center\">
\t\t\t\t\t<h2 class=\"text-medium\">Sorry, This Page Could Not Be Found!</h2>
\t\t\t\t\t<p>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p>
\t\t\t\t\t<a class=\"button\" href=\"";
        // line 90
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["front_page"] ?? null), "html", null, true));
        echo "\">Go Back to Homepage</a>
\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t</div>
\t\t  <!---End content -->
\t\t</div>
\t  </div>

\t</section>
\t<!-- End layout -->

\t";
        // line 104
        if ($this->getAttribute(($context["page"] ?? null), "content_wide", array())) {
            // line 105
            echo "\t<!-- Start content wide -->
\t<section id=\"content-wide\" class=\"content-wide\">        
\t\t";
            // line 107
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content_wide", array()), "html", null, true));
            echo "
\t</section>
\t<!-- End content wide -->
\t";
        }
        // line 111
        echo "
";
        // line 112
        $this->loadTemplate("@wosh/layout/footer.html.twig", "themes/wosh/templates/layout/page--404.html.twig", 112)->display($context);
        // line 113
        echo "
</div>
</div>
";
        // line 116
        if (($context["preloader"] ?? null)) {
            // line 117
            echo "<div class=\"preloader\">
\t<div class=\"preloader-spinner\"></div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/wosh/templates/layout/page--404.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 117,  153 => 116,  148 => 113,  146 => 112,  143 => 111,  136 => 107,  132 => 105,  130 => 104,  113 => 90,  93 => 72,  86 => 68,  82 => 66,  80 => 65,  76 => 63,  66 => 56,  59 => 51,  57 => 50,  54 => 49,  52 => 48,  48 => 47,  43 => 46,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/wosh/templates/layout/page--404.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/themes/wosh/templates/layout/page--404.html.twig");
    }
}
