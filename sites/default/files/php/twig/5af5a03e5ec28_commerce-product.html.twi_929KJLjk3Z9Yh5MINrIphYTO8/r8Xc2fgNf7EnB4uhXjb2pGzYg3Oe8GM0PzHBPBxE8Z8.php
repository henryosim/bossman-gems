<?php

/* themes/wosh/templates/commerce/commerce-product.html.twig */
class __TwigTemplate_b80e79e8dd4c448604fb9d5d5ed7174c9a2edf555a8fe8013554e9d469bfd571 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 23);
        $filters = array("without" => 51, "t" => 58);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set'),
                array('without', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 23
        $context["classes"] = array(0 => "product-post");
        // line 27
        echo "
<div";
        // line 28
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo ">

\t<div class=\"row\">
\t\t<div class=\"col-md-6\">\t
\t\t\t<div class=\"post-image\"><div class=\"product-image slide-carousel owl-carousel\" data-nav=\"true\" data-items=\"1\" data-dots=\"false\" data-autoplay=\"true\" data-autoplayhoverpause=\"true\" data-loop=\"true\">";
        // line 32
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "field_image", array()), "html", null, true));
        echo "</div></div>
\t\t</div>
\t\t<div class=\"col-md-6 product-teaser-wrap\">
\t\t\t
\t\t\t<div class=\"product-price-wrap\">
\t\t\t\t<div class=\"product-price\">";
        // line 37
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "variation_price", array()), "html", null, true));
        echo "</div> <del class=\"product-old-price\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "field_old_price", array()), "html", null, true));
        echo "</del>
\t\t\t</div>
\t\t\t<div class=\"product-brand\">";
        // line 39
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "field_product_brand", array()), "html", null, true));
        echo "</div>
\t\t\t<div class=\"product-title\">";
        // line 40
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "variation_title", array()), "html", null, true));
        echo "</div>
\t\t\t<div class=\"product-short-description\">";
        // line 41
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "field_short_description", array()), "html", null, true));
        echo "</div>
\t\t\t
\t\t\t<div class=\"product-variation-wrap clearfix\">
\t\t\t\t<div class=\"product-add-cart\">";
        // line 44
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "variations", array()), "html", null, true));
        echo "</div>
\t\t\t\t<div class=\"product-flag-icon-wrap text-bottom\">
\t\t\t\t<div class=\"product-flag-icon product-add-wishlist\">";
        // line 46
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "flag_wishlist", array()), "html", null, true));
        echo "</div>
\t\t\t\t<div class=\"product-flag-icon product-add-compare\">";
        // line 47
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "flag_compare", array()), "html", null, true));
        echo "</div>
\t\t\t\t</div>
\t\t\t</div>";
        // line 51
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_without(($context["product"] ?? null), "variation_attributes", "variation_price", "variation_title", "variations", "field_short_description", "field_old_price", "field_image", "field_product_review", "body", "field_product_brand", "flag_wishlist", "flag_compare"), "html", null, true));
        // line 53
        echo "</div>
\t</div>
\t
\t<div class=\"margin-top-50 product-info clearfix\">
\t\t<ul class=\"nav nav-tabs text-center\">
\t\t  <li class=\"active\"><a data-toggle=\"tab\" href=\"#product-detail\">";
        // line 58
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Description")));
        echo "</a></li>
\t\t  <li><a data-toggle=\"tab\" href=\"#product-review\">";
        // line 59
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Reviews")));
        echo "</a></li>
\t\t</ul>

\t\t<div class=\"tab-content\">
\t\t  <div id=\"product-detail\" class=\"tab-pane active\">
\t\t\t<div class=\"product-meta-item\">
\t\t\t<h2 class=\"margin-bottom-20\">";
        // line 65
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Product Description")));
        echo "</h2>
\t\t\t<div>";
        // line 66
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "body", array()), "html", null, true));
        echo "</div>
\t\t\t</div>
\t\t  </div>
\t\t  <div id=\"product-review\" class=\"tab-pane fade\">
\t\t\t";
        // line 70
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["product"] ?? null), "field_product_review", array()), "html", null, true));
        echo "
\t\t  </div>
\t\t</div>
\t</div>

</div>";
    }

    public function getTemplateName()
    {
        return "themes/wosh/templates/commerce/commerce-product.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 70,  124 => 66,  120 => 65,  111 => 59,  107 => 58,  100 => 53,  98 => 51,  93 => 47,  89 => 46,  84 => 44,  78 => 41,  74 => 40,  70 => 39,  63 => 37,  55 => 32,  48 => 28,  45 => 27,  43 => 23,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/wosh/templates/commerce/commerce-product.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/themes/wosh/templates/commerce/commerce-product.html.twig");
    }
}
