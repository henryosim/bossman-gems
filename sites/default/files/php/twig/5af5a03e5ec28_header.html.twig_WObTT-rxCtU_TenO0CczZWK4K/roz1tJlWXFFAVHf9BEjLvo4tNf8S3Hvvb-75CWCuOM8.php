<?php

/* @wosh/layout/header.html.twig */
class __TwigTemplate_d74654ef3f8bc6e013fd21d14822d6af92be3754268cd4854da7d3d74831f96c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 3);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 2
        echo "
";
        // line 3
        if ($this->getAttribute(($context["page"] ?? null), "search", array())) {
            // line 4
            echo "<!-- Modal Search -->
<div id=\"search-modal\" class=\"modal-wrapper modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-lg\" role=\"document\">
    <div class=\"modal-content\">
\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
\t     <div class=\"modal-content-wrap\">
\t\t\t<div class=\"modal-content-holder\">";
            // line 12
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "search", array()), "html", null, true));
            echo "</div>
\t\t</div>

    </div>
  </div>
</div>
";
        }
        // line 19
        echo "\t
<!-- Header Start -->
<header class=\"header\">
\t<div class=\"header-content\">
\t<div class=\"navbar navbar-expand-md navbar-dark\">
\t\t<div class=\"container\">
\t\t
\t\t\t";
        // line 26
        if ($this->getAttribute(($context["page"] ?? null), "branding", array())) {
            // line 27
            echo "\t\t\t<div class=\"navbar-brand header-brand\">
\t\t\t\t";
            // line 28
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "branding", array()), "html", null, true));
            echo "
\t\t\t</div>
\t\t\t";
        }
        // line 31
        echo "
\t\t\t";
        // line 32
        if ($this->getAttribute(($context["page"] ?? null), "primary_menu", array())) {
            // line 33
            echo "\t\t\t<div id=\"main-menu\" class=\"primary-navbar collapse navbar-collapse ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["nav_align"] ?? null), "html", null, true));
            echo "\">
\t\t\t\t";
            // line 34
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "primary_menu", array()), "html", null, true));
            echo "
\t\t\t</div>
\t\t\t";
        }
        // line 37
        echo "\t\t\t\t\t
\t\t\t";
        // line 38
        if ((($this->getAttribute(($context["page"] ?? null), "user_account", array()) || $this->getAttribute(($context["page"] ?? null), "search", array())) || $this->getAttribute(($context["page"] ?? null), "cart", array()))) {
            // line 39
            echo "\t\t\t<div class=\"header-element icon-element\">
\t\t\t\t<div class=\"header-element-icon\">
\t\t\t\t";
            // line 41
            if ($this->getAttribute(($context["page"] ?? null), "user_account", array())) {
                // line 42
                echo "\t\t\t\t\t<div class=\"header-element-item header-element-account\">
\t\t\t\t\t<button type=\"button\" class=\"dropdown-toggle header-icon account-icon\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t<span class=\"ion-ios-gear-outline\"></span>
\t\t\t\t\t</button>
\t\t\t\t\t<div id=\"user-account-block-wrap\" class=\"dropdown-menu user-account-block-wrap\">
\t\t\t\t\t";
                // line 47
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "user_account", array()), "html", null, true));
                echo "
\t\t\t\t\t</div>
\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t";
            }
            // line 51
            echo "\t\t\t\t
\t\t\t\t";
            // line 52
            if ($this->getAttribute(($context["page"] ?? null), "cart", array())) {
                // line 53
                echo "\t\t\t\t\t<div class=\"header-element-item header-element-cart\">
\t\t\t\t\t\t<div class=\"header-icon text-xs-left header-cart\">";
                // line 54
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "cart", array()), "html", null, true));
                echo "</div>
\t\t\t\t\t</div>
\t\t\t\t";
            }
            // line 57
            echo "\t\t\t\t
\t\t\t\t";
            // line 58
            if ($this->getAttribute(($context["page"] ?? null), "search", array())) {
                // line 59
                echo "\t\t\t\t\t<div class=\"header-element-item header-element-search\">
\t\t\t\t\t<button type=\"button\" class=\"header-icon search-icon\" data-toggle=\"modal\" data-target=\"#search-modal\"><span class=\"ion-ios-search-strong\"></span></button>
\t\t\t\t\t</div>
\t\t\t\t";
            }
            // line 63
            echo "\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        }
        // line 66
        echo "\t\t\t
\t\t\t<button class=\"navbar-toggler nav-button\" type=\"button\" data-toggle=\"collapse\" data-target=\"#main-menu\" aria-controls=\"main-menu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
\t\t\t\t<span class=\"navbar-toggler-icon\"></span>
\t\t\t</button>
\t\t
\t\t</div>
\t</div>
\t</div>
</header>
<!-- Header End -->
";
    }

    public function getTemplateName()
    {
        return "@wosh/layout/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 66,  154 => 63,  148 => 59,  146 => 58,  143 => 57,  137 => 54,  134 => 53,  132 => 52,  129 => 51,  122 => 47,  115 => 42,  113 => 41,  109 => 39,  107 => 38,  104 => 37,  98 => 34,  93 => 33,  91 => 32,  88 => 31,  82 => 28,  79 => 27,  77 => 26,  68 => 19,  58 => 12,  48 => 4,  46 => 3,  43 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@wosh/layout/header.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/themes/wosh/templates/layout/header.html.twig");
    }
}
