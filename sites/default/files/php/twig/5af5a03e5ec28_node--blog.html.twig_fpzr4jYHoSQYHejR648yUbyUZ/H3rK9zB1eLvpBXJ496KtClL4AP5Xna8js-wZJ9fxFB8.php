<?php

/* themes/wosh/templates/node/node--blog.html.twig */
class __TwigTemplate_c3807f600dd621fb58eb0a1041d0565aa0b94a89f36044a2e54f35ad7ca61393 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 8, "if" => 19);
        $filters = array("clean_class" => 10, "first" => 24, "render" => 25, "date" => 42, "t" => 95);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if'),
                array('clean_class', 'first', 'render', 'date', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 8
        $context["classes"] = array(0 => "node", 1 => ("node-type-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(        // line 10
($context["node"] ?? null), "bundle", array()))), 2 => "node-content", 3 => "post-wrapper", 4 => (($this->getAttribute(        // line 13
($context["node"] ?? null), "isPromoted", array(), "method")) ? ("node--promoted") : ("")), 5 => (($this->getAttribute(        // line 14
($context["node"] ?? null), "isSticky", array(), "method")) ? ("node--sticky") : ("")), 6 => (( !$this->getAttribute(        // line 15
($context["node"] ?? null), "isPublished", array(), "method")) ? ("node--unpublished") : ("")));
        // line 18
        echo "
";
        // line 19
        if (($context["teaser"] ?? null)) {
            // line 20
            echo "<div";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => "blog-post-teaser post-row"), "method"), "html", null, true));
            echo " >
  ";
            // line 21
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_prefix"] ?? null), "html", null, true));
            echo "
  ";
            // line 22
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_suffix"] ?? null), "html", null, true));
            echo "
<div class=\"post-thumb\">
\t";
            // line 24
            if (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_blog_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "standard")) {
                // line 25
                echo "\t\t";
                if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_image", array()))) {
                    // line 26
                    echo "\t\t\t<div class=\"post-image\">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "html", null, true));
                    echo "</div>
\t\t";
                }
                // line 27
                echo "\t
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 28
($context["content"] ?? null), "field_blog_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "slider")) {
                // line 29
                echo "\t\t<div class=\"post-image\">
\t\t<div class=\"slide-carousel owl-carousel\" data-nav=\"true\" data-items=\"1\" data-dots=\"true\" data-autoplay=\"true\" data-loop=\"true\">
\t\t";
                // line 31
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "
\t\t</div>
\t\t</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 34
($context["content"] ?? null), "field_blog_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "video")) {
                // line 35
                echo "\t\t<div class=\"entry-video\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_video", array()), "html", null, true));
                echo "</div>
\t";
            }
            // line 37
            echo "</div>
\t<div class=\"post-content-wrap\">
\t<div class=\"content-wrap\">
\t";
            // line 40
            if (($context["display_submitted"] ?? null)) {
                // line 41
                echo "\t<div class=\"post-meta\">
\t<div class=\"post-meta-item post-date\"><i class=\"ion-ios-clock-outline\"></i> ";
                // line 42
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["node"] ?? null), "createdtime", array()), "j M Y"), "html", null, true));
                echo "</div>
\t<div class=\"post-meta-item post-category\"><i class=\"ion-ios-folder-outline\"></i> ";
                // line 43
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_category", array()), "html", null, true));
                echo "</div>
\t<div class=\"post-meta-item post-comment\"><i class=\"ion-ios-chatboxes-outline\"></i> ";
                // line 44
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["node"] ?? null), "comment", array()), "comment_count", array()), "html", null, true));
                echo " comments</div>
\t</div>
\t";
            }
            // line 47
            echo "\t<div class=\"post-content\">
\t<div class=\"post-title-wrap\"><h3 class=\"post-title\"><a href=\"";
            // line 48
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["url"] ?? null), "html", null, true));
            echo "\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
            echo "</a></h3></div>
\t<div class=\"post-body\">";
            // line 49
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
            echo "</div>
\t<div class=\"post-link\"><a href=\"";
            // line 50
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["url"] ?? null), "html", null, true));
            echo "\">Read More</a></div>
\t</div>
\t</div>
\t</div>
</div>

";
        } else {
            // line 57
            echo "
<div";
            // line 58
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null), 1 => "blog-post-full"), "method"), "html", null, true));
            echo ">
  ";
            // line 59
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_prefix"] ?? null), "html", null, true));
            echo "
  ";
            // line 60
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_suffix"] ?? null), "html", null, true));
            echo "
\t<div class=\"post-media\">
\t";
            // line 62
            if (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_blog_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "standard")) {
                // line 63
                echo "\t\t";
                if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_image", array()))) {
                    // line 64
                    echo "\t\t\t<div class=\"post-image\">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "html", null, true));
                    echo "</div>
\t\t";
                }
                // line 65
                echo "\t
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 66
($context["content"] ?? null), "field_blog_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "slider")) {
                // line 67
                echo "\t\t<div class=\"post-image\">
\t\t<div class=\"slide-carousel owl-carousel\" data-nav=\"true\" data-items=\"1\" data-dots=\"true\" data-autoplay=\"true\" data-loop=\"true\" data-margin=\"30\">
\t\t";
                // line 69
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "
\t\t</div>
\t\t</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 72
($context["content"] ?? null), "field_blog_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "video")) {
                // line 73
                echo "\t\t<div class=\"entry-video video\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_video", array()), "html", null, true));
                echo "</div>
\t";
            }
            // line 75
            echo "\t</div>
\t<div class=\"post-content\">
\t\t\t<h3 class=\"post-title\">";
            // line 77
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
            echo "</h3>
\t\t\t";
            // line 78
            if (($context["display_submitted"] ?? null)) {
                // line 79
                echo "\t\t\t<div class=\"post-meta\">
\t\t\t\t<div class=\"post-meta-item\"><i class=\"ion-ios-clock-outline\"></i> ";
                // line 80
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["node"] ?? null), "createdtime", array()), "j M Y"), "html", null, true));
                echo "</div>
\t\t\t\t";
                // line 81
                if (($context["author_name"] ?? null)) {
                    // line 82
                    echo "\t\t\t\t<div class=\"post-meta-item\"><i class=\"ion-ios-person-outline\"></i> by ";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["author_name"] ?? null), "html", null, true));
                    echo "</div>
\t\t\t\t";
                }
                // line 84
                echo "\t\t\t\t";
                if ($this->getAttribute(($context["content"] ?? null), "field_category", array())) {
                    // line 85
                    echo "\t\t\t\t<div class=\"post-meta-item\"><i class=\"ion-ios-folder-outline\"></i> ";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_category", array()), "html", null, true));
                    echo "</div>
\t\t\t\t";
                }
                // line 87
                echo "\t\t\t\t";
                if ($this->getAttribute(($context["content"] ?? null), "comment", array())) {
                    // line 88
                    echo "\t\t\t\t<div class=\"post-meta-item\"><i class=\"ion-ios-chatboxes-outline\"></i> ";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["node"] ?? null), "comment", array()), "comment_count", array()), "html", null, true));
                    echo " comments</div>
\t\t\t\t";
                }
                // line 90
                echo "\t\t\t\t
\t\t\t</div>
\t\t\t";
            }
            // line 93
            echo "\t\t\t<div class=\"post-body\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
            echo "</div>
\t\t\t";
            // line 94
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_tags", array()))) {
                // line 95
                echo "\t\t\t<div class=\"post-tags clearfix\"><i class=\"ion-ios-pricetags-outline\"></i> <span>";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Tags:")));
                echo " </span>";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_tags", array()), "html", null, true));
                echo "</div>
\t\t\t";
            }
            // line 97
            echo "\t\t\t
\t\t\t";
            // line 98
            if ($this->getAttribute(($context["content"] ?? null), "comment", array())) {
                // line 99
                echo "\t\t\t\t<div class=\"post-comment\">
\t\t\t\t<div class=\"post-stats\">
\t\t\t\t<span class=\"comment-label\">";
                // line 101
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Comments")));
                echo "</span> <span class=\"comment-count\">( ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["node"] ?? null), "comment", array()), "comment_count", array()), "html", null, true));
                echo " )</span>
\t\t\t\t</div>
\t\t\t\t";
                // line 103
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "comment", array()), "html", null, true));
                echo "
\t\t\t\t</div>
\t\t\t";
            }
            // line 106
            echo "\t</div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/wosh/templates/node/node--blog.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  284 => 106,  278 => 103,  271 => 101,  267 => 99,  265 => 98,  262 => 97,  254 => 95,  252 => 94,  247 => 93,  242 => 90,  236 => 88,  233 => 87,  227 => 85,  224 => 84,  218 => 82,  216 => 81,  212 => 80,  209 => 79,  207 => 78,  203 => 77,  199 => 75,  193 => 73,  191 => 72,  185 => 69,  181 => 67,  179 => 66,  176 => 65,  170 => 64,  167 => 63,  165 => 62,  160 => 60,  156 => 59,  152 => 58,  149 => 57,  139 => 50,  135 => 49,  129 => 48,  126 => 47,  120 => 44,  116 => 43,  112 => 42,  109 => 41,  107 => 40,  102 => 37,  96 => 35,  94 => 34,  88 => 31,  84 => 29,  82 => 28,  79 => 27,  73 => 26,  70 => 25,  68 => 24,  63 => 22,  59 => 21,  54 => 20,  52 => 19,  49 => 18,  47 => 15,  46 => 14,  45 => 13,  44 => 10,  43 => 8,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/wosh/templates/node/node--blog.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/themes/wosh/templates/node/node--blog.html.twig");
    }
}
