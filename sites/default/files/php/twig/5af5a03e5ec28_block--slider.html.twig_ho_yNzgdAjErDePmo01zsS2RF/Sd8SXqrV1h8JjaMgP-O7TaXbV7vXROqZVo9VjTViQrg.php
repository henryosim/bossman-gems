<?php

/* themes/wosh/templates/block/block--slider.html.twig */
class __TwigTemplate_750f8a28b6acc6b1646e1f55b7e224b603b69f5dc8bbd8b2f5d86ef2238ecf02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 9, "if" => 16, "block" => 20);
        $filters = array("clean_class" => 12, "first" => 21);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block'),
                array('clean_class', 'first'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 7
        echo "
";
        // line 9
        $context["classes"] = array(0 => "block", 1 => "slide-block", 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(        // line 12
($context["configuration"] ?? null), "provider", array()))), 3 => ("block-" . \Drupal\Component\Utility\Html::getClass(        // line 13
($context["plugin_id"] ?? null))));
        // line 16
        echo "<div";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo " ";
        if (($context["block_style"] ?? null)) {
            echo "style=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_style"] ?? null), "html", null, true));
            echo "\"";
        }
        echo ">
<div class=\"container-wrap clearfix\">
";
        // line 18
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_prefix"] ?? null), "html", null, true));
        echo "
";
        // line 19
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_suffix"] ?? null), "html", null, true));
        echo "
";
        // line 20
        $this->displayBlock('content', $context, $blocks);
        // line 31
        echo "</div>
</div>";
    }

    // line 20
    public function block_content($context, array $blocks = array())
    {
        // line 21
        echo " ";
        if (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_slide_type", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "slide")) {
            echo " 
\t<div class=\"hero slide-carousel owl-carousel\" data-nav=\"true\" data-items=\"1\" data-dots=\"false\" data-autoplay=\"true\" data-autoplayspeed=\"1500\" data-autoplaytimeout=\"8000\" data-loop=\"true\">
\t\t";
            // line 23
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_slider_block", array()), "html", null, true));
            echo "
\t</div>
 ";
        } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(        // line 25
($context["content"] ?? null), "field_slide_type", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "image")) {
            echo " 
\t<div class=\"slider-image\">
\t\t";
            // line 27
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_slider_block", array()), 0, array()), "html", null, true));
            echo "
\t</div>
 ";
        }
    }

    public function getTemplateName()
    {
        return "themes/wosh/templates/block/block--slider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 27,  92 => 25,  87 => 23,  81 => 21,  78 => 20,  73 => 31,  71 => 20,  67 => 19,  63 => 18,  51 => 16,  49 => 13,  48 => 12,  47 => 9,  44 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/wosh/templates/block/block--slider.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/themes/wosh/templates/block/block--slider.html.twig");
    }
}
