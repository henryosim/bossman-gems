<?php

/* @wosh/layout/footer.html.twig */
class __TwigTemplate_afb33395e9c2e3f1adb690b1a9ed7ba1ac54605d6cb3a958ad9b8056546de6a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 4);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 2
        echo "<footer id=\"footer\" class=\"footer ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["footer_theme"] ?? null), "html", null, true));
        echo "\">
\t<!-- start Footer -->
\t";
        // line 4
        if (((($this->getAttribute(($context["page"] ?? null), "footer_first", array()) || $this->getAttribute(($context["page"] ?? null), "footer_second", array())) || $this->getAttribute(($context["page"] ?? null), "footer_third", array())) || $this->getAttribute(($context["page"] ?? null), "footer_forth", array()))) {
            // line 5
            echo "\t  <div class=\"footer-widget\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t";
            // line 8
            if ($this->getAttribute(($context["page"] ?? null), "footer_first", array())) {
                // line 9
                echo "\t\t\t<!-- Start Footer First Region -->
\t\t\t";
                // line 10
                if (($context["footer_responsive"] ?? null)) {
                    // line 11
                    echo "\t\t\t<div class=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["footer_first_size"] ?? null), "html", null, true));
                    echo " footer-widget-section\">
\t\t\t";
                } else {
                    // line 13
                    echo "\t\t\t<div class=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["footer_class"] ?? null), "html", null, true));
                    echo " footer-widget-section\">
\t\t\t";
                }
                // line 15
                echo "\t\t\t\t";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_first", array()), "html", null, true));
                echo "
\t\t\t</div>
\t\t\t<!-- End Footer First Region -->
\t\t\t";
            }
            // line 19
            echo "
\t\t\t";
            // line 20
            if ($this->getAttribute(($context["page"] ?? null), "footer_second", array())) {
                // line 21
                echo "\t\t\t<!-- Start Footer Second Region -->
\t\t\t";
                // line 22
                if (($context["footer_responsive"] ?? null)) {
                    // line 23
                    echo "\t\t\t<div class=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["footer_second_size"] ?? null), "html", null, true));
                    echo " footer-widget-section\">
\t\t\t";
                } else {
                    // line 25
                    echo "\t\t\t<div class=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["footer_class"] ?? null), "html", null, true));
                    echo " footer-widget-section\">
\t\t\t";
                }
                // line 27
                echo "\t\t\t\t";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_second", array()), "html", null, true));
                echo "
\t\t\t</div>
\t\t\t<!-- End Footer Second Region -->
\t\t\t";
            }
            // line 31
            echo "
\t\t\t";
            // line 32
            if ($this->getAttribute(($context["page"] ?? null), "footer_third", array())) {
                // line 33
                echo "\t\t\t<!-- Start Footer Third Region -->
\t\t\t";
                // line 34
                if (($context["footer_responsive"] ?? null)) {
                    // line 35
                    echo "\t\t\t<div class=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["footer_third_size"] ?? null), "html", null, true));
                    echo " footer-widget-section\">
\t\t\t";
                } else {
                    // line 37
                    echo "\t\t\t<div class=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["footer_class"] ?? null), "html", null, true));
                    echo " footer-widget-section\">
\t\t\t";
                }
                // line 39
                echo "\t\t\t\t";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_third", array()), "html", null, true));
                echo "
\t\t\t</div>
\t\t\t<!-- End Footer Third Region -->
\t\t\t";
            }
            // line 43
            echo "\t\t\t
\t\t\t";
            // line 44
            if ($this->getAttribute(($context["page"] ?? null), "footer_forth", array())) {
                // line 45
                echo "\t\t\t<!-- Start Footer Forth Region -->
\t\t\t";
                // line 46
                if (($context["footer_responsive"] ?? null)) {
                    // line 47
                    echo "\t\t\t<div class=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["footer_forth_size"] ?? null), "html", null, true));
                    echo " footer-widget-section\">
\t\t\t";
                } else {
                    // line 49
                    echo "\t\t\t<div class=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["footer_class"] ?? null), "html", null, true));
                    echo " footer-widget-section\">
\t\t\t";
                }
                // line 51
                echo "\t\t\t\t";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_forth", array()), "html", null, true));
                echo "
\t\t\t</div>
\t\t\t<!-- End Footer Forth Region -->
\t\t\t";
            }
            // line 55
            echo "\t\t\t</div>
\t\t</div>
\t  </div>
\t";
        }
        // line 59
        echo "\t<!--End Footer -->

";
        // line 61
        if (($this->getAttribute(($context["page"] ?? null), "footer_bottom_first", array()) || $this->getAttribute(($context["page"] ?? null), "footer_bottom_second", array()))) {
            // line 62
            echo "\t<div class=\"footer-bottom\">
\t  <div class=\"container\">
\t\t<div class=\"row\">
\t\t  <div class=\"col-md-6\">
\t\t\t";
            // line 66
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_bottom_first", array()), "html", null, true));
            echo "
\t\t  </div>
\t\t  <div class=\"col-md-6\">
\t\t\t";
            // line 69
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_bottom_second", array()), "html", null, true));
            echo "
\t\t  </div>
\t\t</div>
\t  </div>
\t</div>
";
        }
        // line 75
        echo "</footer>
";
        // line 76
        if (($context["scrolltop"] ?? null)) {
            // line 77
            echo "\t<div class=\"back-to-top\"><span class=\"ion-ios-arrow-up\"></span></div>
";
        }
    }

    public function getTemplateName()
    {
        return "@wosh/layout/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 77,  209 => 76,  206 => 75,  197 => 69,  191 => 66,  185 => 62,  183 => 61,  179 => 59,  173 => 55,  165 => 51,  159 => 49,  153 => 47,  151 => 46,  148 => 45,  146 => 44,  143 => 43,  135 => 39,  129 => 37,  123 => 35,  121 => 34,  118 => 33,  116 => 32,  113 => 31,  105 => 27,  99 => 25,  93 => 23,  91 => 22,  88 => 21,  86 => 20,  83 => 19,  75 => 15,  69 => 13,  63 => 11,  61 => 10,  58 => 9,  56 => 8,  51 => 5,  49 => 4,  43 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@wosh/layout/footer.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/themes/wosh/templates/layout/footer.html.twig");
    }
}
