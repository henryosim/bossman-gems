<?php

/* {# inline_template_start #}<div class="blog-post-teaser">
<div class="post-thumb">
{% if field_blog_format == "standard" %}
	{% if field_image | render %}
		<div class="post-image">{{ field_image }}</div>
	{% endif %}	
{% elseif field_blog_format == "slider" %}
     <div class="post-image">
        <div class="slide-carousel owl-carousel" data-nav="true" data-items="1" data-dots="true" data-autoplay="true" data-loop="true">
          {{ field_image_1 }}
        </div>
     </div>
{% elseif field_blog_format == "video" %}
	<div class="entry-video video">{{ field_video }}</div>
{% endif %}
</div>
<div class="post-content-wrap">
<div class="content-wrap">
<div class="post-meta">
<div class="post-meta-item post-date"><i class="ion-ios-clock-outline"></i> {{ created }}</div>
<div class="post-meta-item post-category"><i class="ion-ios-folder-outline"></i> {{ field_category }}</div>
<div class="post-meta-item post-comment"><i class="ion-ios-chatboxes-outline"></i> {{ comment_count }}</div>
</div>
<div class="post-content">
<div class="post-title-wrap"><h4 class="post-title">{{ title }}</h4></div>
<div class="post-link"><a href="{{ link }}">Read More <i class="fa fa-angle-double-right"></i></a></div>
</div>
</div>
</div>
</div> */
class __TwigTemplate_d4e0794973f4e664c6d1f095d3c2f7d49ffcde7f3b35dd781b1411fc0339bfa3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 3);
        $filters = array("render" => 4);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array('render'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"blog-post-teaser\">
<div class=\"post-thumb\">
";
        // line 3
        if ((($context["field_blog_format"] ?? null) == "standard")) {
            // line 4
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["field_image"] ?? null))) {
                // line 5
                echo "\t\t<div class=\"post-image\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_image"] ?? null), "html", null, true));
                echo "</div>
\t";
            }
            // line 6
            echo "\t
";
        } elseif ((        // line 7
($context["field_blog_format"] ?? null) == "slider")) {
            // line 8
            echo "     <div class=\"post-image\">
        <div class=\"slide-carousel owl-carousel\" data-nav=\"true\" data-items=\"1\" data-dots=\"true\" data-autoplay=\"true\" data-loop=\"true\">
          ";
            // line 10
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_image_1"] ?? null), "html", null, true));
            echo "
        </div>
     </div>
";
        } elseif ((        // line 13
($context["field_blog_format"] ?? null) == "video")) {
            // line 14
            echo "\t<div class=\"entry-video video\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_video"] ?? null), "html", null, true));
            echo "</div>
";
        }
        // line 16
        echo "</div>
<div class=\"post-content-wrap\">
<div class=\"content-wrap\">
<div class=\"post-meta\">
<div class=\"post-meta-item post-date\"><i class=\"ion-ios-clock-outline\"></i> ";
        // line 20
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["created"] ?? null), "html", null, true));
        echo "</div>
<div class=\"post-meta-item post-category\"><i class=\"ion-ios-folder-outline\"></i> ";
        // line 21
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_category"] ?? null), "html", null, true));
        echo "</div>
<div class=\"post-meta-item post-comment\"><i class=\"ion-ios-chatboxes-outline\"></i> ";
        // line 22
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["comment_count"] ?? null), "html", null, true));
        echo "</div>
</div>
<div class=\"post-content\">
<div class=\"post-title-wrap\"><h4 class=\"post-title\">";
        // line 25
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
        echo "</h4></div>
<div class=\"post-link\"><a href=\"";
        // line 26
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["link"] ?? null), "html", null, true));
        echo "\">Read More <i class=\"fa fa-angle-double-right\"></i></a></div>
</div>
</div>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"blog-post-teaser\">
<div class=\"post-thumb\">
{% if field_blog_format == \"standard\" %}
\t{% if field_image | render %}
\t\t<div class=\"post-image\">{{ field_image }}</div>
\t{% endif %}\t
{% elseif field_blog_format == \"slider\" %}
     <div class=\"post-image\">
        <div class=\"slide-carousel owl-carousel\" data-nav=\"true\" data-items=\"1\" data-dots=\"true\" data-autoplay=\"true\" data-loop=\"true\">
          {{ field_image_1 }}
        </div>
     </div>
{% elseif field_blog_format == \"video\" %}
\t<div class=\"entry-video video\">{{ field_video }}</div>
{% endif %}
</div>
<div class=\"post-content-wrap\">
<div class=\"content-wrap\">
<div class=\"post-meta\">
<div class=\"post-meta-item post-date\"><i class=\"ion-ios-clock-outline\"></i> {{ created }}</div>
<div class=\"post-meta-item post-category\"><i class=\"ion-ios-folder-outline\"></i> {{ field_category }}</div>
<div class=\"post-meta-item post-comment\"><i class=\"ion-ios-chatboxes-outline\"></i> {{ comment_count }}</div>
</div>
<div class=\"post-content\">
<div class=\"post-title-wrap\"><h4 class=\"post-title\">{{ title }}</h4></div>
<div class=\"post-link\"><a href=\"{{ link }}\">Read More <i class=\"fa fa-angle-double-right\"></i></a></div>
</div>
</div>
</div>
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 26,  130 => 25,  124 => 22,  120 => 21,  116 => 20,  110 => 16,  104 => 14,  102 => 13,  96 => 10,  92 => 8,  90 => 7,  87 => 6,  81 => 5,  78 => 4,  76 => 3,  72 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}<div class=\"blog-post-teaser\">
<div class=\"post-thumb\">
{% if field_blog_format == \"standard\" %}
\t{% if field_image | render %}
\t\t<div class=\"post-image\">{{ field_image }}</div>
\t{% endif %}\t
{% elseif field_blog_format == \"slider\" %}
     <div class=\"post-image\">
        <div class=\"slide-carousel owl-carousel\" data-nav=\"true\" data-items=\"1\" data-dots=\"true\" data-autoplay=\"true\" data-loop=\"true\">
          {{ field_image_1 }}
        </div>
     </div>
{% elseif field_blog_format == \"video\" %}
\t<div class=\"entry-video video\">{{ field_video }}</div>
{% endif %}
</div>
<div class=\"post-content-wrap\">
<div class=\"content-wrap\">
<div class=\"post-meta\">
<div class=\"post-meta-item post-date\"><i class=\"ion-ios-clock-outline\"></i> {{ created }}</div>
<div class=\"post-meta-item post-category\"><i class=\"ion-ios-folder-outline\"></i> {{ field_category }}</div>
<div class=\"post-meta-item post-comment\"><i class=\"ion-ios-chatboxes-outline\"></i> {{ comment_count }}</div>
</div>
<div class=\"post-content\">
<div class=\"post-title-wrap\"><h4 class=\"post-title\">{{ title }}</h4></div>
<div class=\"post-link\"><a href=\"{{ link }}\">Read More <i class=\"fa fa-angle-double-right\"></i></a></div>
</div>
</div>
</div>
</div>", "");
    }
}
