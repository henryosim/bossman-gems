<?php

/* themes/wosh/templates/user/user.html.twig */
class __TwigTemplate_da5fe53355562657b92c19296703b358aec1106ed99d9b58705fecb2d8243894 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 21);
        $filters = array("without" => 33);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array('without'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 19
        echo "<div";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => "profile"), "method"), "html", null, true));
        echo ">

  ";
        // line 21
        if ((($context["content"] ?? null) && ((($context["view_mode"] ?? null) == "default") || (($context["view_mode"] ?? null) == "full")))) {
            // line 22
            echo "    <div class=\"user-header\">
      <div class=\"inner\">
        <div class=\"row\">
          <div class=\"col-md-4\">
              <div class=\"user-profile-picture\">
                ";
            // line 27
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "user_picture", array()), "html", null, true));
            echo "
              </div>
          </div>
          <div class=\"col-md-8\">
\t\t    <h2 class=\"user-name\">";
            // line 31
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_name", array()), "html", null, true));
            echo " @";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["name"] ?? null), "html", null, true));
            echo "</h2>
\t\t\t<div class=\"user-content\">
            ";
            // line 33
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_without(($context["content"] ?? null), "user_picture", "field_name", "field_facebook", "field_twitter", "field_linkedin", "field_youtube"), "html", null, true));
            echo "
\t\t\t
                ";
            // line 35
            if (((($this->getAttribute(($context["content"] ?? null), "field_facebook", array()) || $this->getAttribute(($context["content"] ?? null), "field_twitter", array())) || $this->getAttribute(($context["content"] ?? null), "field_linkedin", array())) || $this->getAttribute(($context["content"] ?? null), "field_youtube", array()))) {
                // line 36
                echo "                  <div class=\"team-social user-social-links\">
                    <a href=\"";
                // line 37
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_facebook", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\"><i class=\"fa fa-facebook\"></i></a>
\t\t\t\t\t<a href=\"";
                // line 38
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_twitter", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\"><i class=\"fa fa-twitter\"></i></a>
\t\t\t\t\t<a href=\"";
                // line 39
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_linkedin", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\"><i class=\"fa fa-linkedin\"></i></a>
\t\t\t\t\t<a href=\"";
                // line 40
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_youtube", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\"><i class=\"fa fa-youtube\"></i></a>
                  </div>
                ";
            }
            // line 43
            echo "          </div>
\t\t  </div>
        </div>
      </div>

    </div>

  ";
        } else {
            // line 51
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["content"] ?? null), "html", null, true));
        }
        // line 53
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "themes/wosh/templates/user/user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 53,  110 => 51,  100 => 43,  94 => 40,  90 => 39,  86 => 38,  82 => 37,  79 => 36,  77 => 35,  72 => 33,  65 => 31,  58 => 27,  51 => 22,  49 => 21,  43 => 19,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/wosh/templates/user/user.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/themes/wosh/templates/user/user.html.twig");
    }
}
