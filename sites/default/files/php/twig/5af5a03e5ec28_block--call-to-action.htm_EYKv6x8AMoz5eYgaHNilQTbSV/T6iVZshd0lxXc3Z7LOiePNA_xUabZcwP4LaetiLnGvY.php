<?php

/* themes/wosh/templates/block/block--call-to-action.html.twig */
class __TwigTemplate_a53c46e5a92da3588e6caefe9866be178b96a7c4752a5d3f524d81a0b3b695e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 9, "if" => 17, "block" => 21);
        $filters = array("clean_class" => 12, "first" => 23, "render" => 39);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block'),
                array('clean_class', 'first', 'render'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 7
        echo "
";
        // line 9
        $context["classes"] = array(0 => "block", 1 => "action-block", 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(        // line 12
($context["configuration"] ?? null), "provider", array()))), 3 => ("block-" . \Drupal\Component\Utility\Html::getClass(        // line 13
($context["plugin_id"] ?? null))));
        // line 16
        echo "<div class=\"cta-";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_cta_format", array()), "#items", array(), "array"), "value", array()), "html", null, true));
        echo "\">
<div";
        // line 17
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo " ";
        if (($context["block_style"] ?? null)) {
            echo "style=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_style"] ?? null), "html", null, true));
            echo "\"";
        }
        echo ">
<div class=\"container-wrap clearfix\">
  ";
        // line 19
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_prefix"] ?? null), "html", null, true));
        echo "
  ";
        // line 20
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_suffix"] ?? null), "html", null, true));
        echo "
  ";
        // line 21
        $this->displayBlock('content', $context, $blocks);
        // line 55
        echo "</div>
</div>
</div>";
    }

    // line 21
    public function block_content($context, array $blocks = array())
    {
        // line 22
        echo "
 ";
        // line 23
        if (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_cta_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "style1")) {
            echo " 
\t<div class=\"cta-wrap action-block-1 text-center\">
\t  ";
            // line 25
            if (($context["label"] ?? null)) {
                // line 26
                echo "\t\t<div class=\"block-title-wrap clearfix\">
\t\t<div class=\"block-title-content\">
\t\t";
                // line 28
                if ((($context["block_title_style"] ?? null) == "block-title-2")) {
                    // line 29
                    echo "\t\t";
                    if (($context["block_subtitle"] ?? null)) {
                        echo "<h5 class=\"block-subtitle\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_subtitle"] ?? null), "html", null, true));
                        echo "</h5>";
                    }
                    // line 30
                    echo "\t\t<h2";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["title_attributes"] ?? null), "addClass", array(0 => "block-title"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h2>
\t\t";
                } else {
                    // line 32
                    echo "\t\t<h2";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["title_attributes"] ?? null), "addClass", array(0 => "block-title"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h2>
\t\t";
                    // line 33
                    if (($context["block_subtitle"] ?? null)) {
                        echo "<h5 class=\"block-subtitle\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_subtitle"] ?? null), "html", null, true));
                        echo "</h5>";
                    }
                    // line 34
                    echo "\t\t";
                }
                // line 35
                echo "\t\t</div>
\t\t</div>
\t  ";
            }
            // line 38
            echo "\t\t<div class=\"cta-text margin-bottom-20\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
            echo "</div>
\t\t";
            // line 39
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_link", array()))) {
                // line 40
                echo "\t\t<div class=\"nav menu cta-link\"><a href=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\" class=\"button\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#title", array(), "array"), "html", null, true));
                echo "</a></div>
\t\t";
            }
            // line 42
            echo "\t</div>

 ";
        } else {
            // line 44
            echo " 
\t<div class=\"cta-wrap action-block-2 row\">
\t\t<div class=\"col-sm-9 cta-text\">";
            // line 46
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
            echo "</div>
\t\t";
            // line 47
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_link", array()))) {
                // line 48
                echo "\t\t<div class=\"nav menu col-sm-3 text-middle cta-link\"><a href=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\" class=\"button\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#title", array(), "array"), "html", null, true));
                echo "</a></div>
\t\t";
            }
            // line 50
            echo "\t</div>

 ";
        }
        // line 53
        echo "
  ";
    }

    public function getTemplateName()
    {
        return "themes/wosh/templates/block/block--call-to-action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 53,  176 => 50,  168 => 48,  166 => 47,  162 => 46,  158 => 44,  153 => 42,  145 => 40,  143 => 39,  138 => 38,  133 => 35,  130 => 34,  124 => 33,  117 => 32,  109 => 30,  102 => 29,  100 => 28,  96 => 26,  94 => 25,  89 => 23,  86 => 22,  83 => 21,  77 => 55,  75 => 21,  71 => 20,  67 => 19,  56 => 17,  51 => 16,  49 => 13,  48 => 12,  47 => 9,  44 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/wosh/templates/block/block--call-to-action.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/themes/wosh/templates/block/block--call-to-action.html.twig");
    }
}
