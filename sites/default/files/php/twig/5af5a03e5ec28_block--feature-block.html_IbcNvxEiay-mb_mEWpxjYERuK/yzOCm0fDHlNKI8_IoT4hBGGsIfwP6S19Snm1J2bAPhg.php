<?php

/* themes/wosh/templates/block/block--feature-block.html.twig */
class __TwigTemplate_53154f42be18f25d7c09a3b28f49afc17574e03ebdec97a5d2f1c71646c51b8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 8, "if" => 15, "block" => 22);
        $filters = array("clean_class" => 11, "first" => 15, "render" => 62);
        $functions = array("attach_library" => 16, "file_url" => 31);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block'),
                array('clean_class', 'first', 'render'),
                array('attach_library', 'file_url')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 8
        $context["classes"] = array(0 => "block", 1 => "feature-block", 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(        // line 11
($context["configuration"] ?? null), "provider", array()))), 3 => ("block-" . \Drupal\Component\Utility\Html::getClass(        // line 12
($context["plugin_id"] ?? null))));
        // line 15
        if (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "video-popup")) {
            // line 16
            echo "  ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("wosh/colorbox"), "html", null, true));
            echo "
";
        }
        // line 18
        echo "<div";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo " ";
        if (($context["block_style"] ?? null)) {
            echo "style=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_style"] ?? null), "html", null, true));
            echo "\"";
        }
        echo ">
<div class=\"container-wrap feature-block-";
        // line 19
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_feature_layout", array()), "#items", array(), "array"), "value", array()), "html", null, true));
        echo " clearfix\">
  ";
        // line 20
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_prefix"] ?? null), "html", null, true));
        echo "
  ";
        // line 21
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_suffix"] ?? null), "html", null, true));
        echo "
  ";
        // line 22
        $this->displayBlock('content', $context, $blocks);
        // line 229
        echo "</div>
</div>";
    }

    // line 22
    public function block_content($context, array $blocks = array())
    {
        // line 23
        echo "  
 <div class=\"row-wrap block-content\">
";
        // line 25
        if (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_feature_layout", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "layout1")) {
            // line 26
            echo "\t<div class=\"row feature-block-wrap feature-block-image-left\">
\t<div class=\"col-12 col-md-6 feat-block-image-wrap ";
            // line 27
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "value", array()), "html", null, true));
            echo "\">
\t";
            // line 28
            if (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "static-image")) {
                // line 29
                echo "\t<div class=\"feat-block-image\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 30
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "background-image")) {
                // line 31
                echo "\t<div class=\"feat-block-image\" style=\"background-image: url(";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#item", array(), "array"), "entity", array()), "uri", array()), "value", array()))), "html", null, true));
                echo ");\">
\t\t<div class=\"feat-block-image-bg\">";
                // line 32
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 34
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "slider-image")) {
                // line 35
                echo "\t<div class=\"feat-block-image feat-block-slide\">
\t\t<div class=\"slide-carousel owl-carousel\" data-nav=\"false\" data-items=\"1\" data-dots=\"true\" data-autoplay=\"true\" data-loop=\"true\">
\t\t";
                // line 37
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "
\t\t</div>
\t</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 40
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "video-popup")) {
                // line 41
                echo "\t<div class=\"feat-block-image feat-block-video\" style=\"background-image: url(";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#item", array(), "array"), "entity", array()), "uri", array()), "value", array()))), "html", null, true));
                echo ");\">
\t\t<div class=\"feat-block-image-bg\">";
                // line 42
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t\t<a class=\"colorbox video-colorbox\" href=\"";
                // line 43
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_video", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\"><span class=\"video-link ti-control-play\"></span></a>
\t</div>
\t";
            }
            // line 46
            echo "\t</div>
\t<div class=\"col-12 col-md-6 feat-block-content\">
\t<div class=\"feat-content-wrap\">
\t";
            // line 49
            if (($context["label"] ?? null)) {
                // line 50
                echo "\t<div class=\"block-title-wrap\">
\t<div class=\"block-title-content\">
\t";
                // line 52
                if ((($context["block_title_style"] ?? null) == "block-title-2")) {
                    // line 53
                    echo "\t";
                    if (($context["block_subtitle"] ?? null)) {
                        echo "<h5 class=\"block-subtitle\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_subtitle"] ?? null), "html", null, true));
                        echo "</h5>";
                    }
                    // line 54
                    echo "\t<h2";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["title_attributes"] ?? null), "addClass", array(0 => "block-title"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h2>
\t";
                } else {
                    // line 56
                    echo "\t<h2";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["title_attributes"] ?? null), "addClass", array(0 => "block-title"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h2>
\t";
                    // line 57
                    if (($context["block_subtitle"] ?? null)) {
                        echo "<h5 class=\"block-subtitle\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_subtitle"] ?? null), "html", null, true));
                        echo "</h5>";
                    }
                    // line 58
                    echo "\t";
                }
                // line 59
                echo "\t</div>
\t</div>
\t";
            }
            // line 62
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "body", array()))) {
                echo "<div class=\"feat-block-text\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
                echo "</div>";
            }
            // line 63
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_block", array()))) {
                // line 64
                echo "\t<div class=\"text-left block-right\">
\t";
                // line 65
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_block", array()), "html", null, true));
                echo "
\t</div>
\t";
            }
            // line 68
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_link", array()))) {
                // line 69
                echo "\t<a class=\"button button-default\" href=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#title", array(), "array"), "html", null, true));
                echo "</a>
\t";
            }
            // line 71
            echo "\t</div>
\t</div>
\t</div>

";
        } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(        // line 75
($context["content"] ?? null), "field_feature_layout", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "layout2")) {
            // line 76
            echo "\t<div class=\"row feature-block-wrap feature-block-image-right\">
\t<div class=\"col-12 col-md-6 feat-block-content\">
\t<div class=\"feat-content-wrap\">
\t";
            // line 79
            if (($context["label"] ?? null)) {
                // line 80
                echo "\t<div class=\"block-title-wrap\">
\t<div class=\"block-title-content\">
\t";
                // line 82
                if ((($context["block_title_style"] ?? null) == "block-title-2")) {
                    // line 83
                    echo "\t";
                    if (($context["block_subtitle"] ?? null)) {
                        echo "<h5 class=\"block-subtitle\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_subtitle"] ?? null), "html", null, true));
                        echo "</h5>";
                    }
                    // line 84
                    echo "\t<h2";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["title_attributes"] ?? null), "addClass", array(0 => "block-title"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h2>
\t";
                } else {
                    // line 86
                    echo "\t<h2";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["title_attributes"] ?? null), "addClass", array(0 => "block-title"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h2>
\t";
                    // line 87
                    if (($context["block_subtitle"] ?? null)) {
                        echo "<h5 class=\"block-subtitle\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_subtitle"] ?? null), "html", null, true));
                        echo "</h5>";
                    }
                    // line 88
                    echo "\t";
                }
                // line 89
                echo "\t</div>
\t</div>
\t";
            }
            // line 92
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "body", array()))) {
                echo "<div class=\"feat-block-text\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
                echo "</div>";
            }
            // line 93
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_block", array()))) {
                // line 94
                echo "\t<div class=\"text-left block-right\">
\t";
                // line 95
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_block", array()), "html", null, true));
                echo "
\t</div>
\t";
            }
            // line 98
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_link", array()))) {
                // line 99
                echo "\t<a class=\"button button-default\" href=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#title", array(), "array"), "html", null, true));
                echo "</a>
\t";
            }
            // line 101
            echo "\t</div>
\t</div>
\t<div class=\"col-12 col-md-6 feat-block-image-wrap ";
            // line 103
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "value", array()), "html", null, true));
            echo "\">
\t";
            // line 104
            if (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "static-image")) {
                // line 105
                echo "\t<div class=\"feat-block-image\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 106
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "background-image")) {
                // line 107
                echo "\t<div class=\"feat-block-image\" style=\"background-image: url(";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#item", array(), "array"), "entity", array()), "uri", array()), "value", array()))), "html", null, true));
                echo ");\">
\t\t<div class=\"feat-block-image-bg\">";
                // line 108
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 110
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "slider-image")) {
                // line 111
                echo "\t<div class=\"feat-block-image feat-block-slide\">
\t\t<div class=\"slide-carousel owl-carousel\" data-nav=\"false\" data-items=\"1\" data-dots=\"true\" data-autoplay=\"true\" data-loop=\"true\">
\t\t";
                // line 113
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "
\t\t</div>
\t</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 116
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "video-popup")) {
                // line 117
                echo "\t<div class=\"feat-block-image feat-block-video\" style=\"background-image: url(";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#item", array(), "array"), "entity", array()), "uri", array()), "value", array()))), "html", null, true));
                echo ");\">
\t\t<div class=\"feat-block-image-bg\">";
                // line 118
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t\t<a class=\"colorbox video-colorbox\" href=\"";
                // line 119
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_video", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\"><span class=\"video-link ti-control-play\"></span></a>
\t</div>
\t";
            }
            // line 122
            echo "\t</div>
\t</div>
\t
";
        } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(        // line 125
($context["content"] ?? null), "field_feature_layout", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "layout3")) {
            // line 126
            echo "\t<div class=\"row feature-block-wrap feature-block-image-left\">
\t<div class=\"col-12 col-md-5 feat-block-image-wrap ";
            // line 127
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "value", array()), "html", null, true));
            echo "\">
\t";
            // line 128
            if (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "static-image")) {
                // line 129
                echo "\t<div class=\"feat-block-image\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 130
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "background-image")) {
                // line 131
                echo "\t<div class=\"feat-block-image\" style=\"background-image: url(";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#item", array(), "array"), "entity", array()), "uri", array()), "value", array()))), "html", null, true));
                echo ");\">
\t\t<div class=\"feat-block-image-bg\">";
                // line 132
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 134
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "slider-image")) {
                // line 135
                echo "\t<div class=\"feat-block-image feat-block-slide\">
\t\t<div class=\"slide-carousel owl-carousel\" data-nav=\"false\" data-items=\"1\" data-dots=\"true\" data-autoplay=\"true\" data-loop=\"true\">
\t\t";
                // line 137
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "
\t\t</div>
\t</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 140
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "video-popup")) {
                // line 141
                echo "\t<div class=\"feat-block-image feat-block-video\" style=\"background-image: url(";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#item", array(), "array"), "entity", array()), "uri", array()), "value", array()))), "html", null, true));
                echo ");\">
\t\t<div class=\"feat-block-image-bg\">";
                // line 142
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t\t<a class=\"colorbox video-colorbox\" href=\"";
                // line 143
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_video", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\"><span class=\"video-link ti-control-play\"></span></a>
\t</div>
\t";
            }
            // line 146
            echo "\t</div>
\t<div class=\"col-12 col-md-7 feat-block-content\">
\t<div class=\"feat-content-wrap\">
\t";
            // line 149
            if (($context["label"] ?? null)) {
                // line 150
                echo "\t<div class=\"block-title-wrap\">
\t<div class=\"block-title-content\">
\t";
                // line 152
                if ((($context["block_title_style"] ?? null) == "block-title-2")) {
                    // line 153
                    echo "\t";
                    if (($context["block_subtitle"] ?? null)) {
                        echo "<h5 class=\"block-subtitle\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_subtitle"] ?? null), "html", null, true));
                        echo "</h5>";
                    }
                    // line 154
                    echo "\t<h2";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["title_attributes"] ?? null), "addClass", array(0 => "block-title"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h2>
\t";
                } else {
                    // line 156
                    echo "\t<h2";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["title_attributes"] ?? null), "addClass", array(0 => "block-title"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h2>
\t";
                    // line 157
                    if (($context["block_subtitle"] ?? null)) {
                        echo "<h5 class=\"block-subtitle\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_subtitle"] ?? null), "html", null, true));
                        echo "</h5>";
                    }
                    // line 158
                    echo "\t";
                }
                // line 159
                echo "\t</div>
\t</div>
\t";
            }
            // line 162
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "body", array()))) {
                echo "<div class=\"feat-block-text\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
                echo "</div>";
            }
            // line 163
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_block", array()))) {
                // line 164
                echo "\t<div class=\"text-left block-right feat-block-content-alt\">
\t";
                // line 165
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_block", array()), "html", null, true));
                echo "
\t</div>
\t";
            }
            // line 168
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_link", array()))) {
                // line 169
                echo "\t<a class=\"button button-default\" href=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#title", array(), "array"), "html", null, true));
                echo "</a>
\t";
            }
            // line 171
            echo "\t</div>
\t</div>
\t</div>\t
\t
";
        } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(        // line 175
($context["content"] ?? null), "field_feature_layout", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "layout4")) {
            // line 176
            echo "\t<div class=\"row feature-block-wrap feature-block-image-right\">
\t<div class=\"col-12 col-md-7 feat-block-content\">
\t<div class=\"feat-content-wrap\">
\t";
            // line 179
            if (($context["label"] ?? null)) {
                // line 180
                echo "\t<div class=\"block-title-wrap\">
\t<div class=\"block-title-content\">
\t";
                // line 182
                if ((($context["block_title_style"] ?? null) == "block-title-2")) {
                    // line 183
                    echo "\t";
                    if (($context["block_subtitle"] ?? null)) {
                        echo "<h5 class=\"block-subtitle\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_subtitle"] ?? null), "html", null, true));
                        echo "</h5>";
                    }
                    // line 184
                    echo "\t<h2";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["title_attributes"] ?? null), "addClass", array(0 => "block-title"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h2>
\t";
                } else {
                    // line 186
                    echo "\t<h2";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["title_attributes"] ?? null), "addClass", array(0 => "block-title"), "method"), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
                    echo "</h2>
\t";
                    // line 187
                    if (($context["block_subtitle"] ?? null)) {
                        echo "<h5 class=\"block-subtitle\">";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["block_subtitle"] ?? null), "html", null, true));
                        echo "</h5>";
                    }
                    // line 188
                    echo "\t";
                }
                // line 189
                echo "\t</div>
\t</div>
\t";
            }
            // line 192
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "body", array()))) {
                echo "<div class=\"feat-block-text\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
                echo "</div>";
            }
            // line 193
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_block", array()))) {
                // line 194
                echo "\t<div class=\"text-left block-right feat-block-content-alt\">
\t";
                // line 195
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_block", array()), "html", null, true));
                echo "
\t</div>
\t";
            }
            // line 198
            echo "\t";
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_link", array()))) {
                // line 199
                echo "\t<a class=\"button button-default\" href=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", array()), 0, array()), "#title", array(), "array"), "html", null, true));
                echo "</a>
\t";
            }
            // line 201
            echo "\t</div>
\t</div>
\t<div class=\"col-12 col-md-5 feat-block-image-wrap ";
            // line 203
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "value", array()), "html", null, true));
            echo "\">
\t";
            // line 204
            if (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "static-image")) {
                // line 205
                echo "\t<div class=\"feat-block-image\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 206
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "background-image")) {
                // line 207
                echo "\t<div class=\"feat-block-image\" style=\"background-image: url(";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#item", array(), "array"), "entity", array()), "uri", array()), "value", array()))), "html", null, true));
                echo ");\">
\t\t<div class=\"feat-block-image-bg\">";
                // line 208
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 210
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "slider-image")) {
                // line 211
                echo "\t<div class=\"feat-block-image feat-block-slide\">
\t\t<div class=\"slide-carousel owl-carousel\" data-nav=\"false\" data-items=\"1\" data-dots=\"true\" data-autoplay=\"true\" data-loop=\"true\">
\t\t";
                // line 213
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "
\t\t</div>
\t</div>
\t";
            } elseif (($this->getAttribute(twig_first($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(            // line 216
($context["content"] ?? null), "field_media_format", array()), "#items", array(), "array"), "getValue", array(), "method")), "value", array()) == "video-popup")) {
                // line 217
                echo "\t<div class=\"feat-block-image feat-block-video\" style=\"background-image: url(";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", array()), 0, array()), "#item", array(), "array"), "entity", array()), "uri", array()), "value", array()))), "html", null, true));
                echo ");\">
\t\t<div class=\"feat-block-image-bg\">";
                // line 218
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
                echo "</div>
\t\t<a class=\"colorbox video-colorbox\" href=\"";
                // line 219
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_video", array()), 0, array()), "#url", array(), "array"), "html", null, true));
                echo "\"><span class=\"video-link ti-control-play\"></span></a>
\t</div>
\t";
            }
            // line 222
            echo "\t</div>
\t</div>\t

";
        }
        // line 226
        echo "\t
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/wosh/templates/block/block--feature-block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  662 => 226,  656 => 222,  650 => 219,  646 => 218,  641 => 217,  639 => 216,  633 => 213,  629 => 211,  627 => 210,  622 => 208,  617 => 207,  615 => 206,  610 => 205,  608 => 204,  604 => 203,  600 => 201,  592 => 199,  589 => 198,  583 => 195,  580 => 194,  577 => 193,  570 => 192,  565 => 189,  562 => 188,  556 => 187,  549 => 186,  541 => 184,  534 => 183,  532 => 182,  528 => 180,  526 => 179,  521 => 176,  519 => 175,  513 => 171,  505 => 169,  502 => 168,  496 => 165,  493 => 164,  490 => 163,  483 => 162,  478 => 159,  475 => 158,  469 => 157,  462 => 156,  454 => 154,  447 => 153,  445 => 152,  441 => 150,  439 => 149,  434 => 146,  428 => 143,  424 => 142,  419 => 141,  417 => 140,  411 => 137,  407 => 135,  405 => 134,  400 => 132,  395 => 131,  393 => 130,  388 => 129,  386 => 128,  382 => 127,  379 => 126,  377 => 125,  372 => 122,  366 => 119,  362 => 118,  357 => 117,  355 => 116,  349 => 113,  345 => 111,  343 => 110,  338 => 108,  333 => 107,  331 => 106,  326 => 105,  324 => 104,  320 => 103,  316 => 101,  308 => 99,  305 => 98,  299 => 95,  296 => 94,  293 => 93,  286 => 92,  281 => 89,  278 => 88,  272 => 87,  265 => 86,  257 => 84,  250 => 83,  248 => 82,  244 => 80,  242 => 79,  237 => 76,  235 => 75,  229 => 71,  221 => 69,  218 => 68,  212 => 65,  209 => 64,  206 => 63,  199 => 62,  194 => 59,  191 => 58,  185 => 57,  178 => 56,  170 => 54,  163 => 53,  161 => 52,  157 => 50,  155 => 49,  150 => 46,  144 => 43,  140 => 42,  135 => 41,  133 => 40,  127 => 37,  123 => 35,  121 => 34,  116 => 32,  111 => 31,  109 => 30,  104 => 29,  102 => 28,  98 => 27,  95 => 26,  93 => 25,  89 => 23,  86 => 22,  81 => 229,  79 => 22,  75 => 21,  71 => 20,  67 => 19,  56 => 18,  50 => 16,  48 => 15,  46 => 12,  45 => 11,  44 => 8,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/wosh/templates/block/block--feature-block.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/themes/wosh/templates/block/block--feature-block.html.twig");
    }
}
