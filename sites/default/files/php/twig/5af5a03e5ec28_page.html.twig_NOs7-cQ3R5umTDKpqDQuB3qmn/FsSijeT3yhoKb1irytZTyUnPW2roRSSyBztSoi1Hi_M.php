<?php

/* themes/wosh/templates/layout/page.html.twig */
class __TwigTemplate_0ce187acfbb961a99bce7c0df44387c9aabfad5eaae9664b1dbe7589ef694a40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("include" => 48, "if" => 50);
        $filters = array("render" => 89);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('include', 'if'),
                array('render'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 46
        echo "<div id=\"wrapper\" class=\"wrapper ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["site_layout"] ?? null), "html", null, true));
        echo "\">
<div class=\"layout-wrap ";
        // line 47
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["sticky_header"] ?? null), "html", null, true));
        echo "\">
\t";
        // line 48
        $this->loadTemplate("@wosh/layout/header.html.twig", "themes/wosh/templates/layout/page.html.twig", 48)->display($context);
        // line 49
        echo "
\t";
        // line 50
        if ($this->getAttribute(($context["page"] ?? null), "slider", array())) {
            // line 51
            echo "\t<!-- Slider -->
\t<section id=\"slider\" class=\"clearfix\">        
\t\t<div id=\"slider-wrap\">
\t\t\t<div class=\"container-fluid\">
\t\t\t\t<div class=\"row\">
\t\t\t\t";
            // line 56
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "slider", array()), "html", null, true));
            echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</section>
\t<!-- End Slider -->
\t";
        }
        // line 63
        echo "
\t";
        // line 64
        if (($this->getAttribute(($context["page"] ?? null), "page_title", array()) &&  !($context["is_front"] ?? null))) {
            // line 65
            echo "\t<!-- Page Title -->
\t<section id=\"page-title\" class=\"page-title ";
            // line 66
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_background"] ?? null), "html", null, true));
            echo "\">
\t\t<div class=\"container\">
\t\t\t";
            // line 68
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "page_title", array()), "html", null, true));
            echo "
\t\t\t";
            // line 69
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "breadcrumb", array()), "html", null, true));
            echo "
\t\t</div>
\t</section>
\t  <!-- End Page Title -->
\t";
        }
        // line 74
        echo "

\t";
        // line 76
        if ($this->getAttribute(($context["page"] ?? null), "content_wide_top", array())) {
            // line 77
            echo "\t<!-- Start content top -->
\t<section id=\"content-wide-top\" class=\"content-wide\">        
\t\t";
            // line 79
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content_wide_top", array()), "html", null, true));
            echo "
\t</section>
\t<!-- End content top -->
\t";
        }
        // line 83
        echo "
\t";
        // line 84
        if ((( !twig_test_empty($this->getAttribute(($context["page"] ?? null), "content", array())) || $this->getAttribute(($context["page"] ?? null), "sidebar_first", array())) || $this->getAttribute(($context["page"] ?? null), "sidebar_second", array()))) {
            // line 85
            echo "\t<!-- layout -->
\t<section id=\"page-wrapper\" class=\"page-wrapper\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row content-layout\">
\t\t\t  ";
            // line 89
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["page"] ?? null), "sidebar_first", array()))) {
                // line 90
                echo "\t\t\t  <!--- Start Left SideBar -->
\t\t\t\t<div class =\"";
                // line 91
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["sidebarfirst"] ?? null), "html", null, true));
                echo "  sidebar\">
\t\t\t\t\t";
                // line 92
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_first", array()), "html", null, true));
                echo "
\t\t\t\t</div>
\t\t\t  <!---End Right SideBar -->
\t\t\t  ";
            }
            // line 96
            echo "
\t\t\t  ";
            // line 97
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["page"] ?? null), "content", array()))) {
                // line 98
                echo "\t\t\t  <!--- Start content -->
\t\t\t\t<div class=\"";
                // line 99
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["contentlayout"] ?? null), "html", null, true));
                echo "  main-content\">
\t\t\t\t\t";
                // line 100
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
                echo "
\t\t\t\t</div>
\t\t\t  <!---End content -->
\t\t\t  ";
            }
            // line 104
            echo "
\t\t\t  ";
            // line 105
            if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["page"] ?? null), "sidebar_second", array()))) {
                // line 106
                echo "\t\t\t  <!--- Start Right SideBar -->
\t\t\t\t<div class=\"";
                // line 107
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["sidebarsecond"] ?? null), "html", null, true));
                echo "  sidebar\">
\t\t\t\t\t";
                // line 108
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_second", array()), "html", null, true));
                echo "
\t\t\t\t</div>
\t\t\t  <!---End Right SideBar -->
\t\t\t  ";
            }
            // line 112
            echo "\t\t\t</div>
\t\t</div>
\t</section>
\t<!-- End layout -->
\t";
        }
        // line 117
        echo "
\t";
        // line 118
        if ($this->getAttribute(($context["page"] ?? null), "content_wide", array())) {
            // line 119
            echo "\t<!-- Start content wide -->
\t<section id=\"content-wide\" class=\"content-wide\">        
\t\t";
            // line 121
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content_wide", array()), "html", null, true));
            echo "
\t</section>
\t<!-- End content wide -->
\t";
        }
        // line 125
        echo "
\t";
        // line 126
        $this->loadTemplate("@wosh/layout/footer.html.twig", "themes/wosh/templates/layout/page.html.twig", 126)->display($context);
        // line 127
        echo "</div>
</div>
";
        // line 129
        if (($context["preloader"] ?? null)) {
            // line 130
            echo "<div class=\"preloader\">
\t<div class=\"preloader-spinner\"></div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/wosh/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 130,  215 => 129,  211 => 127,  209 => 126,  206 => 125,  199 => 121,  195 => 119,  193 => 118,  190 => 117,  183 => 112,  176 => 108,  172 => 107,  169 => 106,  167 => 105,  164 => 104,  157 => 100,  153 => 99,  150 => 98,  148 => 97,  145 => 96,  138 => 92,  134 => 91,  131 => 90,  129 => 89,  123 => 85,  121 => 84,  118 => 83,  111 => 79,  107 => 77,  105 => 76,  101 => 74,  93 => 69,  89 => 68,  84 => 66,  81 => 65,  79 => 64,  76 => 63,  66 => 56,  59 => 51,  57 => 50,  54 => 49,  52 => 48,  48 => 47,  43 => 46,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/wosh/templates/layout/page.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/themes/wosh/templates/layout/page.html.twig");
    }
}
