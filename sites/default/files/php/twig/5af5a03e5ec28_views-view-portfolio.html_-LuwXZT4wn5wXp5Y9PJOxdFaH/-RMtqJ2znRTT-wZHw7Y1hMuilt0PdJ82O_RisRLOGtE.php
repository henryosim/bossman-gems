<?php

/* modules/custom/portfolio/templates/views-view-portfolio.html.twig */
class __TwigTemplate_251ff7c961407ccffc473a7795477095a4e98d76f1d08207d71a625759ece691 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 22, "for" => 42, "set" => 44);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if', 'for', 'set'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 20
        echo "
<div ";
        // line 21
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["wrapper_class"] ?? null), "html", null, true));
        echo ">
  ";
        // line 22
        if (($context["title"] ?? null)) {
            // line 23
            echo "    <h3>";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
            echo "</h3>
  ";
        }
        // line 25
        echo "
  ";
        // line 26
        if (($context["filter_options"] ?? null)) {
            // line 27
            echo "  <div class=\"clearfix shuffle-filters-wrap\">
    <div class=\"shuffle-filters-container\">
      ";
            // line 29
            if (($context["filter_label"] ?? null)) {
                // line 30
                echo "        <div class=\"filter-label\">";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["filter_label"] ?? null), "html", null, true));
                echo "</div>
      ";
            }
            // line 32
            echo "      <ul class=\"shuffle-filters filter-options\">
\t  <li class=\"shuffle-filters-all shuffle-filters-item active\" data-group=\"all\">All</li>
\t  </ul>
      ";
            // line 35
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["filter_options"] ?? null), "html", null, true));
            echo "
    </div>
  </div>
  ";
        }
        // line 39
        echo "
  ";
        // line 41
        echo "  <div class=\"shuffle-container\">
    ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 43
            echo "      ";
            // line 44
            $context["row_classes"] = array(0 => ((            // line 45
($context["default_row_class"] ?? null)) ? ("views-row") : ("")));
            // line 48
            echo "
      <div";
            // line 49
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["row"], "attributes", array()), "addClass", array(0 => ($context["row_classes"] ?? null), 1 => ($context["grid_classes"] ?? null)), "method"), "html", null, true));
            echo ">
        ";
            // line 50
            if ($this->getAttribute($context["row"], "content", array())) {
                // line 51
                echo "          <div class=\"shuffle-inner\">
          ";
                // line 52
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["row"], "content", array()), "html", null, true));
                echo "
          </div>
        ";
            }
            // line 55
            echo "      </div>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/custom/portfolio/templates/views-view-portfolio.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 58,  120 => 55,  114 => 52,  111 => 51,  109 => 50,  105 => 49,  102 => 48,  100 => 45,  99 => 44,  97 => 43,  93 => 42,  90 => 41,  87 => 39,  80 => 35,  75 => 32,  69 => 30,  67 => 29,  63 => 27,  61 => 26,  58 => 25,  52 => 23,  50 => 22,  46 => 21,  43 => 20,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modules/custom/portfolio/templates/views-view-portfolio.html.twig", "/Users/henryosim/Projects/bossmangemsandjewellery/drupal/modules/custom/portfolio/templates/views-view-portfolio.html.twig");
    }
}
